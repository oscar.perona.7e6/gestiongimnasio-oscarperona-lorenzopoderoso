# Project ASMA
![Alt Text](/img/Project-Asma-_1_.gif)
<hr>

Esta app desarrollada por **Oscar Perona** y **Lorenzo Poderoso** para poder gestionar el gimnasio del grupo **ASMA**.

<hr>

## ¿Qué función tiene Project ASMA?

Project ASMA es una aplicación para gestionar los grupos de gimnasio **ASMA**.

* ### Creación de nuevo usuario
    Te permitirá poder introducir nuevos usuarios a tu base de datos. Tambien, te permite migrar una base de datos antigua a una moderna sin problemas.

* ### Modificar usuario
    Como dice su nombre, modificar usuario te permitirá poder modificar los campos de usuario. (Nombre, telefono...)

* ### Bloquear o desbloquear usuario
    Hemos añadido la opción de poder restringir el acceso a tu gimnasio bloqueando a los usuarios.

* ### Hacer backup
    Si eres una persona despistada y no quieres perder nada de lo que has modificado esta opción te ayudará. Hemos implementado esta función para poder guardar todo lo modificado. También hemos añadido que al salir sin guardar haga un backup antes de cerrar el programa.

<hr>

## Como instalarlo
![Alt Text](/img/downloadApp.gif)

* Para descargarlo vaya al botón de descarga que está justo a la izquierda del botón azúl "Clone".

* Abra su IDE preferido y seleccione el archivo descargado para poder ejecutarlo.

* Una vez su programa esté cargado, pulse al botón Play de su IDE para poder iniciar la App.

<hr>




