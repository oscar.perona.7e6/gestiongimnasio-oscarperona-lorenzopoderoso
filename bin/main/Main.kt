import java.io.File
import java.nio.file.Path
import java.text.SimpleDateFormat
import java.util.*
import kotlin.io.path.*
import kotlin.system.exitProcess
/*
   * TITLE: Sistema de alta y gestión de usuarios de un gimnasio del grupo ASMA
   * NAME: Lorenzo Poderoso, Oscar Perona
 */

val scanner = Scanner(System.`in`)
var lastId = 0
val newDataPath = Path("data/userData.txt")
var dailyBackup = false


/**
 * Inicia todas las funciones de Project Asma
 * @author Lorenzo y Oscar
 */
fun main() {
    checkImportFiles()
    updateLastID()
    menu()
}


/**
 * Comprobará si el directorio "import" tiene archivos
 * @author Lorenzo y Oscar
 */
fun checkImportFiles() {
    val path = Path("import/")
    val filesOfDirectory : List<Path> = path.listDirectoryEntries()
    if (filesOfDirectory.isNotEmpty()) writeNewData(path,)
}

/**
 * La función writeNewData se encarga de obtener los datos de la carpeta import y los pasa a la nueva base de datos.
 * Hemos creado una variable global "newDataPath" que lee el archivo "userData.txt"
 * @author Oscar y Lorenzo
 */
fun writeNewData(path: Path){
    for (file in path.listDirectoryEntries()){
        val linesOfFile = file.readLines()
        for(line in linesOfFile){

            val userInfoParts = line.replace(";",",").split(",").toMutableList()
            val userIsActive = userInfoParts[4]
            val userIsBlocked = userInfoParts[5]

            if (userIsActive == "true" && userIsBlocked == "false") {
                updateLastID()
                lastId++
                userInfoParts[0] = lastId.toString()
                newDataPath.appendText(userInfoParts.toString().replace("[","").replace("]","").replace(",",";"))
                newDataPath.appendText("\n")
            }
        //file.deleteIfExists()
        }
    }
}

/**
 * Esta función se encargará de almacenar cual es la última ID escrita en el fichero "userData.txt"
 * @author Lorenzo y Oscar
 */
fun updateLastID(){
    val file = newDataPath.readLines()
    for (line in file){
        val lineList = line.split(";").toMutableList()
        if(lineList[0].toInt()>lastId) lastId = lineList[0].toInt()
    }
}

/**
 * Muestra por pantalla un menú que te dará elegir entre las diferentes opciones del programa.
 * @author Oscar
 */
fun menu(){

    println("**********************")
    println("*    PROJECT ASMA    *")
    println("**********************")
    println()
    println("1. Crear nuevo usuario")
    println("2. Modificar usuario")
    println("3. Des/Bloquear usuario")
    println("4. Hacer backup")
    println("5. Salir")
    repeat(2){
        println()
    }
    println("Introduce una opcion del menu")
    val menuOpction = scanner.nextInt()

    when(menuOpction){
        1 -> insertUser()
        2 -> modifyUser()
        3 -> blockUser()
        4 -> {
            val skipMenu = false
            backup(skipMenu)
        }
        5 -> {
            if (dailyBackup){
                println("Saliendo del programa...")
                exitProcess(0)
            }
            else{
                println("Backup diario no realizado, se procedera a realizar un backup")
                val skipMenu = true
                backup(skipMenu)
                println("Saliendo del programa... ")
                exitProcess(0)
            }

        }
        else -> {
            println("No existe ninguna opcion con ese numero")
            menu()
        }
    }
}

/**
 * insertUser se encarga de obtener la ultima id del fichero "userData.txt" y introducir un usuario.
 * @author Lorenzo y Oscar
 */
fun insertUser(){
    val newUser = mutableListOf<String>()
    var newUserName = ""
    var newPhoneNumber = ""
    var newMail = ""
    var correctNewData = 0

    do {
        if (newUserName == ""){
            println("Introduzca su nombre y primer apellido: ")
            val import = readLine()!!
            if (import.length<2) println("Nombre de usuario no valido")
            else {
                newUserName = import
                correctNewData++
            }
        }
        if (newPhoneNumber==""){
            println("Introduzca un telefono: ")
            val import = readLine()!!
            if (import.length!=9) println("Numero de telefono no valido")
            else {
                newPhoneNumber = import
                correctNewData++
            }
        }
        if (newMail==""){
            println("Introduzca un correo electronico: ")
            val import = readLine()!!
            if ('@' !in import) println("Mail no valido")
            else {
                newMail = import
                correctNewData++
            }
        }
    }while (correctNewData<3)

    lastId++
    newUser.add(lastId.toString())
    newUser.add(newUserName)
    newUser.add(newPhoneNumber)
    newUser.add(newMail)
    newUser.add("true")
    newUser.add("false;")
    newDataPath.appendText(newUser.toString().replace("[","").replace("]","").replace(",",";"))
    newDataPath.appendText("\n")
    updateLastID()
    menu()
}

/**
 * Preguntará por el usuario que desea modificar mediante su ID y sobreescribirá los datos de dicho usuario.
 * @author Oscar y Lorenzo
 */
fun modifyUser(){
    println("Escribe el id del usuario a modificar")
    val id = scanner.nextInt()
    val file = newDataPath.readLines()

    if(id in 0..lastId){

        println("Introduce el nuevo nombre del usuario")
        val newNameUser = readLine()!!
        println("Introduce el nuevo numero de telefono")
        val newPhoneNum = readLine()!!
        println("Introduce el nuevo mail")
        val newMail = readLine()!!

        val line = file[id-1].split(";")
        val userIsActive = line[4]
        val userIsBlocked = line[5]

        val modifiedUser = "$id; $newNameUser; $newPhoneNum; $newMail;$userIsActive;$userIsBlocked;"
        newDataPath.writeText("")

        for (user in file){
            if (file.indexOf(user) == id-1) {
                newDataPath.appendText(modifiedUser)
                newDataPath.appendText("\n")
            }
            else {
                newDataPath.appendText(user)
                newDataPath.appendText("\n")
            }
        }
        println("Usuario modificado con exito")
        updateLastID()
        menu()
    }
    else {
        println("No existe ningun usuario con la id $id")
        modifyUser()
    }
}

/**
 * Pregunta una ID y bloquea o desbloquea al usuario con esa ID según en que estado este.
 * @author Lorenzo y Oscar
 */
fun blockUser() {
    println("¿Que ID quieres des/bloquear?: ")
    val id = scanner.nextInt()
    val file = newDataPath.readLines()

    if(id in 0..lastId){ //Comprobación de que existe el ID
        val line = file[id-1].split(";")
        val userIsBlocked = line[5].toBoolean()
        var newBlock = true

        if (userIsBlocked) newBlock = false

        val blockedUser = "$id;${line[1]};${line[2]};${line[3]};${line[4]}; $newBlock;"
        newDataPath.writeText("")

        for (user in file){
            if (file.indexOf(user) == id-1) {
                newDataPath.appendText(blockedUser)
                newDataPath.appendText("\n")
            }
            else {
                newDataPath.appendText(user)
                newDataPath.appendText("\n")
            }
        }
        println("Usuario des/blockeado con exito")
        updateLastID()
        menu()
    }
    else {
        println("No existe ningun usuario con la id $id")
        blockUser()
    }
}

/**
 * Comprovara si en el directorio "backup" existe algun backup con la fecha del dia actual, si no existe creara uno y si existe lo sobrescribira
 * @author Lorenzo y Oscar
 */
fun backup(skipMenu:Boolean){

    val dateFormat = SimpleDateFormat("dd-MM-yyyy")
    val date = Date()
    val currentDate = dateFormat.format(date)

    val backupFile = File("backups/${currentDate}userData.txt")
    val dataFile = File("data/userData.txt")

    dailyBackup = true
    if (backupFile.exists()) {
        backupFile.delete()
        dataFile.copyTo(backupFile)
    }
    else {
        dataFile.copyTo(backupFile)
    }
    println("Backup realizado con exito")
    if (!skipMenu) menu()
}

